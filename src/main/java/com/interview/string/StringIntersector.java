package com.interview.string;

import java.util.Set;
import java.util.stream.Collectors;

public class StringIntersector {

    public String f(String a, String b) {
        if(a == null || b == null || a.isEmpty()|| b.isEmpty())
            return "";

        //Convert to Character Set using java-8 streams and Lamda
        Set<Character> setA = a.chars()
                .mapToObj(c -> ((char) c))
                .collect(Collectors.toSet());

        Set<Character> setB = b.chars()
                .mapToObj(c -> ((char) c))
                .collect(Collectors.toSet());

        //This operation effectively modifies this set so that
        //its value is the intersection of the two sets.
        setA.retainAll(setB);

        StringBuilder result = new StringBuilder();
        setA.forEach(result::append);

        return result.toString();

    }
}
