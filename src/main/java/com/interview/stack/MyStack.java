package com.interview.stack;

//Stack built using LinkedLists
public class MyStack<T> {

    //LinkedList node
    private class Node {
        private T data;
        private Node next;

        public Node(T data, Node next) {
            this.data = data;
            this.next = next;
        }
    }

    //To keep track of size of stack
    private int size = 0;

    //Points to head of linkedList
    private Node head;

    public void push(T data) {
        head = new Node(data, head);
        size++;
    }

    public T pop() {
        if(head == null)
            return null;

        Node current = head;
        head = current.next;
        current.next = null;

        size--;

        return current.data;
    }

    public boolean isEmpty() {
        return size == 0 && head == null;
    }

    public int size() {
        return size;
    }
}

