package com.interview.word;

public class WordOrderReversal {

    public String reverse(String sentence) {

        if(sentence == null || sentence.isEmpty())
            return "";

        if(!sentence.contains(" "))
            return sentence;

        char[] charArray = sentence.toCharArray();

        //Reverse the whole string in the first pass
        reverseCharArrayInPlace(charArray, 0, charArray.length - 1);

        //Reverser the individual words in the second pass
        int startIndex = 0;
        for(int endIndex = 0; endIndex <= charArray.length; endIndex++) {
            //if we hit a space or end of sentence then we have a word, reverse it back!
            if(endIndex == charArray.length || charArray[endIndex] == ' ') {
                //reverse the word
                reverseCharArrayInPlace(charArray, startIndex, endIndex-1);
                //move the start index to start of new word
                startIndex = endIndex + 1;
            }
        }

        return  new String(charArray);
    }

    private void reverseCharArrayInPlace(char[] inputArray, int startIndex, int endIndex) {

        //Swap character from both ends and keep moving inwards.
        //When we reach the middle, array would be reversed.
        while(endIndex > startIndex) {
            //Swap characters at both ends
            char temp = inputArray[startIndex];
            inputArray[startIndex] = inputArray[endIndex];
            inputArray[endIndex] = temp;

            //move inwards
            startIndex++;
            endIndex--;
        }
    }
}
