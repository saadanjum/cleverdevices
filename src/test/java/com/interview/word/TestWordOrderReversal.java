package com.interview.word;

import org.junit.Before;
import org.junit.Test;

import java.lang.ref.SoftReference;

import static org.junit.Assert.assertEquals;

public class TestWordOrderReversal {

    private WordOrderReversal wordOrderReversal;

    @Before
    public void setup() {
        wordOrderReversal = new WordOrderReversal();
    }

    @Test
    public void withEmptyString() {
        String reversedSentence = wordOrderReversal.reverse("");

        assertEquals("", reversedSentence);
    }

    @Test
    public void withSingleWord() {
        String sentence = "single-word";
        String reversedSentence = wordOrderReversal.reverse(sentence);

        assertEquals("single-word", reversedSentence);
    }

    @Test
    public void withTwoWords() {
        String sentence = "Two Words";
        String reversedSentence = wordOrderReversal.reverse(sentence);

        assertEquals("Words Two", reversedSentence);
    }

    @Test
    public void withMultipleWords() {
        String sentence = "word1 word2 word3 word4 word5";
        String reversedSentence = wordOrderReversal.reverse(sentence);

        assertEquals("word5 word4 word3 word2 word1", reversedSentence);
    }

    @Test
    public void withSentenceStartingWithSpace() {
        String sentence = " word1 word2 word3 word4 word5";
        String reversedSentence = wordOrderReversal.reverse(sentence);

        assertEquals("word5 word4 word3 word2 word1 ", reversedSentence);
    }

    @Test
    public void withSentenceEndingWithSpace() {
        String sentence = "word1 word2 word3 word4 word5 ";
        String reversedSentence = wordOrderReversal.reverse(sentence);

        assertEquals(" word5 word4 word3 word2 word1", reversedSentence);
    }

    @Test
    public void withSentenceStartingAndEndingWithSpace() {
        String sentence = " word1 word2 word3 word4 word5 ";
        String reversedSentence = wordOrderReversal.reverse(sentence);

        assertEquals(" word5 word4 word3 word2 word1 ", reversedSentence);
    }

    @Test
    public void withSentenceMultipleSpaces() {
        String sentence = "word1 word2    word3 word4 word5 ";
        String reversedSentence = wordOrderReversal.reverse(sentence);

        assertEquals(" word5 word4 word3    word2 word1", reversedSentence);
    }

}
