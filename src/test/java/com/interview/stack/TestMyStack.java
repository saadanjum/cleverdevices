package com.interview.stack;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestMyStack {

    private MyStack<Integer> stack;

    @Before
    public void setup() {
        stack = new MyStack<>();
    }

    @Test
    public void withNoData() {
        assertTrue(stack.isEmpty());
    }

    @Test
    public void withOneElementPushed() {
        stack.push(1);

        assertEquals( 1, stack.size());
    }

    @Test
    public void withTwoElementsPushed() {
        stack.push(1);
        stack.push(2);

        assertEquals( 2, stack.size());
    }

    @Test
    public void withEmptyPop() {
        assertNull(stack.pop());
    }

    @Test
    public void withOnePushThenPop() {
        Integer data = 1;

        stack.push(data);
        assertEquals( 1, stack.size());

        Integer poppedData = stack.pop();
        assertEquals( 0, stack.size());
        assertEquals(data, poppedData);
    }

    @Test
    public void withTwoPushThenPop() {
        Integer data = 1;
        stack.push(data);
        assertEquals( 1, stack.size());

        data = 2;
        stack.push(data);
        assertEquals( 2, stack.size());

        Integer poppedData = stack.pop();
        assertEquals( 1, stack.size());
        assertEquals(data, poppedData);
    }

    @Test
    public void withThreePushThenTwoPop() {
       Integer data1 = 1;
        stack.push(data1);
        assertEquals( 1, stack.size());

        Integer data2 = 2;
        stack.push(data2);
        assertEquals( 2, stack.size());

        Integer data3 = 3;
        stack.push(data3);
        assertEquals( 3, stack.size());

        Integer poppedData = stack.pop();
        assertEquals( 2, stack.size());
        assertEquals(data3, poppedData);

        poppedData = stack.pop();
        assertEquals( 1, stack.size());
        assertEquals(data2, poppedData);
    }

    @Test
    public void withThreePushThenThreePop() {
        Integer data1 = 1;
        stack.push(data1);
        assertEquals( 1, stack.size());

        Integer data2 = 2;
        stack.push(data2);
        assertEquals( 2, stack.size());

        Integer data3 = 3;
        stack.push(data3);
        assertEquals( 3, stack.size());

        Integer poppedData = stack.pop();
        assertEquals( 2, stack.size());
        assertEquals(data3, poppedData);

        poppedData = stack.pop();
        assertEquals( 1, stack.size());
        assertEquals(data2, poppedData);

        poppedData = stack.pop();
        assertEquals( 0, stack.size());
        assertEquals(data1, poppedData);
        assertTrue(stack.isEmpty());
    }

}
