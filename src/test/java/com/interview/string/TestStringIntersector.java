package com.interview.string;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStringIntersector {

    private StringIntersector stringIntersector;

    @Before
    public void setup() {
        stringIntersector = new StringIntersector();
    }

    @Test
    public void withNullInput() {
        String result = stringIntersector.f(null, null);
        assertEquals("", result);
    }

    @Test
    public void withBothEmptyStrings() {
        String result = stringIntersector.f("", "");
        assertEquals("", result);
    }

    @Test
    public void withOneEmptyStrings() {
        String result = stringIntersector.f("", "abc");
        assertEquals("", result);
    }

    @Test
    public void withSingleCharacter() {
        String result = stringIntersector.f("a", "b");
        assertEquals("", result);
    }

    @Test
    public void withCommonSingleCharacter() {
        String result = stringIntersector.f("a", "a");
        assertEquals("a", result);
    }

    @Test
    public void withNoCommonCharacter() {
        String result = stringIntersector.f("abcdef", "ghijk");
        assertEquals("", result);
    }

    @Test
    public void withSomeCommonCharacter() {
        String result = stringIntersector.f("abcdef", "xayzeabefgh");
        assertEquals("abef", result);
    }
}
