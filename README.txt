
To compile and run tests:

- Windows:
    .\mvnw.cmd clean compile test

- Linux / Unix
    ./mvnw clean compile test
